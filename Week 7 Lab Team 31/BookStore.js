<<<<<<< HEAD
=======
let listOfAllKnownAuthors = []

class BookStore
{
>>>>>>> master
// Array that keeps track of all known authors who have their books sold in the bookstore
let listOfAllKnownAuthors = []

// BookStore Class Definition which contains the necessary code and documentation for the creation and modification of a bookstore
class BookStore
{
    
     /*******************************************************************************************
     Constructor function that creates a new instance of a bookstore

      Inputs:
      - Three Strings (bookstore name, address of the store and the owner of the store)
      Returns:
      - A new instance of a bookstore
     *****************************************************************************************/
    constructor(name, address, owner)
    {
        this._name = name;
        this._address = address;
        this._owner = owner;
        this._booksAvailable = [];
        this._totalCopiesOfAllBooks = 0
    }

    /*******************************************************************************************
      Function which takes a author's Name as an argument and checks if there is a preexisting entry
      in the array containing the database of author names

      Inputs:
      - A String (authorName)
      Returns:
      - A boolean variable (True if the author is found, and False if the author is not)
     *****************************************************************************************/
    authorKnown(authorName)
    {
        let foundThem = false;
        //for loop to query the array of all known authors to check if the author in question exists
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                //sets the local variable foundThem to true if the author is found
                foundThem = true
            }
        }
        return foundThem
    }

    /*******************************************************************************************
      Function which processes a new book upon addition to the bookstore

      Inputs:
      - A book object (bookInstance) and a numerical value (copies of the book)
      Returns:
      - Adds onto the known authors if new, else just creates an entry for the book and pushes it into an array (booksAvaliable)
      - Increments the counter that keeps track of all copies of books by 1
     *****************************************************************************************/
    addBook(bookInstance, copies)
    {
        // sets the index of the book (if present in the booksAvaliable Array) to the variable positionOfBook
        let positionOfBook = this.checkForBook(bookInstance);
        //if branch will trigger if the book exists (as positionOfBook would have been set in previous line)
        if (positionOfBook != null)
        {
            //local variable foundBook which holds the instance of the book (in the booksAvailable array)
             let foundBook = this._booksAvailable[positionOfBook];
             //increments the copies property of foundBooks by the number passed into the function
             foundBook.copies += copies;
             console.log("Added " + copies + " copies of " + foundBook.book);
        }
        // else branch creates the book as it does not previously exist in the booksAvailable database
        else
        {
            //creates an object which has two properties, the book instance and the number of copies
             let bookCopies = {
                 book: bookInstance,
                 copies: copies
             };
             this._booksAvailable.push(bookCopies);
             console.log("Added " + copies + " copies of a new book: " + bookInstance);
        }

        this._totalCopiesOfAllBooks += copies;
    }

    /*******************************************************************************************
      Function which checks if a book is avalible for purchase (removal from database).
      And handles it accordingly.

      Inputs:
      - A book object (bookInstance) and a numerical value (number of that book to sell)
      Returns:
      - Removes the book /sfrom the database(booksAvaliable) if there are sufficent copies,
      else prints out that there are insufficent copies/ or not found if the book does not exist
     *****************************************************************************************/
    sellBook(bookInstance, numberSold)
    {
         // sets the index of the book (if present in the booksAvaliable Array) to the variable positionOfBook
        let positionOfBook = this.checkForBook(bookInstance);
        //if branch will trigger if the book exists (as positionOfBook would have been set in previous line)
        if (positionOfBook != null)
        {
            //local variable foundBook which holds the instance of the book (in the booksAvailable array)
            let foundBook = this._booksAvailable[positionOfBook];
            //if branch will trigger when there are sufficient copies of the book within the database
            if (numberSold > this._booksAvailable[positionOfBook].copies)
            {
                console.log("Not enough copies of " + foundBook.book + " to sell");
            }
            //else branch will trigger when there are sufficient copies for sale
            else
            {
                //decrements the copies property of the book object by the number passed into the function (numberSold)
                foundBook.copies -= numberSold;
                //if branch will trigger if there the last copy of the book is sold (hence there are no more left)
                if (foundBook.copies === 0)
                {
                    this._booksAvailable.pop(PositionOfBook);
                    this._NumTitles -= 1;
                    for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
                    {
                        if (authorName === listOfAllKnownAuthors[pos])
                        {
                            listOfAllKnownAuthors.pop(pos);
                        }
                   } 
                }
                //decrements the total number of books by the number passed into the function (numberSold)
                this._totalCopiesOfAllBooks -= numberSold;
                console.log("Sold " + numberSold + " copies of " + foundBook.book);
            }
        }
        //else branch will trigger if the book does not exist
        else
        {
            console.log(bookInstance + " not found");
        }
    }

    /*******************************************************************************************
      Function which checks if a book is exists within the database(booksAvailable), and if so
      checks for the number of copies present.

      Inputs:
      - bookInstance (which book it is) 
      Returns:
      - The index location of that book (if it exists). Else returns null.
     *****************************************************************************************/
    checkForBook(bookInstance)
    {
        //local variable currBookNum which serves as a counter variable
        let currBookNum = 0;
        //while loops checks the entire array of booksAvailable if the book (bookInstance) is preset
        while (currBookNum < this._booksAvailable.length)
        {
            //if branch will trigger if the book is found and will return the index of that book and break out of the function
            if (this._booksAvailable[currBookNum].book.isTheSame(bookInstance))
            {
                return currBookNum;
            }
<<<<<<< HEAD
            
=======
            //else branch should not contain the return keyword as that would break from this loop and fucntion ealier than what we want. TTOOOOOOOOOOO BEEEEEEEEEE DELETEEEEEEEEEEEDDDDDDDDDDDDDDDDDDDDDDDDD
            else
            {
                return null;
            }
>>>>>>> master
            //increments the counter by 1
            currBookNum += 1;
        }
        //this line will execute if the book is not found within the database (booksAvailable)
        return null;
    }

    //getter method to obtain the name of the bookstore instance
    get name()
    {
        return this._name;
    }

    //setter method to set a new name for the bookstore instance
    set name(newName)
    {
        this._name = newName;
    }

    //getter method to obtain the address of the owner of the bookstore instance
    get address()
    {
        return this._address;
    }

    //setter method to set the address of the owner of the bookstore instance
    set address(newAddress)
    {
        this._address = newAddress;
    }

    //getter method to obtain the name of the owner of the bookstore instance
    get owner()
    {
        return this._owner;
    }

    //Setter method to set the name of the owner of the bookstore instance
    set address(newOwner)
    {
        this._owner = newOwner;
    }
}



// Class for the information of the books.
class Book
{
    constructor(title, author, publicationYear, price)
    //private attribute
    {
        this._title = title; //The title of the books
        this._author = author; //The author of the book 
        this._publicationYear = publicationYear; //The publication year of the books
        this._price = price;//The price of the books
        if (this.authorKnown(this._author) === false)
        {
            listOfAllKnownAuthors.push(this._author)
        }
    }
//return the  price of the book


    isTheSame(otherBook)
    {
        return otherBook.price === this.price;
    }

//using the position in the list of all known author array to find the corresponding author 
    authorKnown(authorName)
    {
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true;
            }
        }
        return foundThem;
    }
<<<<<<< HEAD
    
    //Setter method to set the title of the book 
    set title(newTitle)
    {
        this._title = newTitle;
    }
    
=======
>>>>>>> master
    //return the title of books
    get title()
    {
        return this._title;
    }

<<<<<<< HEAD
    //Setter method to set the author of the book 
    set author(newAuthor)
    {
        this._author = newAuthor;
    }
    
=======
>>>>>>> master
    //return the author of books
    get author()
    {
        return this._author;
    }
<<<<<<< HEAD
    
    //Setter method to set the publication year of the book 
    set publicationYear(newPublicationYear)
    {
        this._publicationYear = newPublicationYear;
    }
    
=======

>>>>>>> master
    //return the publication year of books
    get publicationYear()
    {
        return this._publicationYear;
    }
<<<<<<< HEAD
        
    //Setter method to set the price of the book 
    set price(newPrice)
    {
        this._price = newPrice;
    }
    
=======

>>>>>>> master
    //return the price of books
    get price()
    {
        return this._price;
    }

    //convert the information of the book to a message and return
    toString()
    {
        return this.title + ", " + this.author + ". " + this.publicationYear + " ($" + this.price + ")";
    }
}

// Book details courtesy of Harry Potter series by J.K. Rowling
let cheapSpellBook = new Book("The idiot's guide to spells","Morlan",2005,40);
let flourishAndBlotts = new BookStore("Flourish & Blotts", "North side, Diagon Alley, London, England", "unknown");
let monsterBook = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let monsterBookToSell = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let spellBook = new Book("The Standard Book of Spells, Grade 4", "Miranda Goshawk", 1921, 80);
flourishAndBlotts.addBook(cheapSpellBook,1000);
flourishAndBlotts.addBook(monsterBook, 500);
flourishAndBlotts.sellBook(monsterBookToSell, 200);
flourishAndBlotts.addBook(spellBook, 40);
flourishAndBlotts.addBook(spellBook, 20);
flourishAndBlotts.sellBook(spellBook, 15);
flourishAndBlotts.addBook(monsterBookToSell, -30);
flourishAndBlotts.sellBook(monsterBookToSell, 750);

console.log("Authors known: " + listOfAllKnownAuthors);
