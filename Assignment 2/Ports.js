//Port class
class Port{
    constructor(name,country,type,size,latitude,longitude){
        this.Name=name;
        this.Country=country;
        this.Type=type;
        this.Size=size;
        this.latitude=latitude;
        this.longitude=longitude;
    }
    
}
class PortsList{
    constructor()
    {
        this.listOfPorts = [];
    }
};

function createPorts()
{
    let portName = document.getElementById("portName").value;
    let Country = document.getElementById("Country").value;
    let typePort = document.getElementById("typePort").value;
    let sizePort = document.getElementById("sizePort").value;
    let locPrecision = document.getElementById("locPrecision").value;
    let latitude = document.getElementById("latitude").value;
    let longitude = document.getElementById("longitude").value;
    console.log("Port: ", portName);
    console.log("country: ", Country);
    console.log("Type: ", Type);
    console.log("size: ", sizePort);
    console.log("location presion: " , locPrecision);
    console.log("latitude: ",latitude);
    console.log("longitude: ",longitude);
    tempPort = new Port (portName);
    tempPort.Name = portName;
    tempPort.Country = Country;
    tempPort.Type = typePort;
    tempPort.Size = sizePort;
    tempPort.location = locPrecision;
    tempPort.latitude = latitude;
    tempPort.longitude = longitude;
    savePort(tempPort)
    loadPort()
}

function savePort(Port1)
{
    let tempRouteJSON = JSON.stringify(Port1);
    localStorage.setItem("Test", tempRouteJSON);
}
function loadPort()
{
    routePDO = JSON.parse(localStorage.getItem("Test"));
    console.log("This is JSON", portPDO);
    loadedRoute = initialiseFromRoutePDO(portsPDO);
    console.log(loadedPort);
}

function initialiseFromPortPDO(portPDO)
{
    tempPort = new Port(portPDO.name);
    tempPort.Name = portPDO.portPDOportName;
    tempPort.Country = portPDO.Country;
    tempPort.Type = portPDO.typePort;
    tempPort.Size = portPDO.sizePort;
    tempPort.latitude = portPDO.latitude;
    tempPort.longitude = portPDO.longitude;
    return tempPort;
}